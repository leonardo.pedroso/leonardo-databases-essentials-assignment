module mongo

go 1.16

require (
	github.com/google/uuid v1.3.0 // direct
	github.com/gorilla/mux v1.8.0 // direct
	go.mongodb.org/mongo-driver v1.7.0 // direct
)
