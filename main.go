package main

import (
	"encoding/json"
	"net/http"

	model "mongo/model"
	"mongo/mongo"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/createAccount", createAccount).Methods("POST")
	r.HandleFunc("/listAccounts", listAccounts).Methods("GET")
	http.ListenAndServe("127.0.0.1:8000", r)
}

func createAccount(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var newUser model.User
	err := decoder.Decode(&newUser)

	if err != nil {
		code := http.StatusBadRequest
		http.Error(w, err.Error(), code)
		return
	}

	newUser.Id = uuid.NewString()
	result, err := mongo.CreateUser(newUser)
	if err != nil {
		code := http.StatusNotAcceptable
		http.Error(w, err.Error(), code)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(result.(string)))

}
func listAccounts(w http.ResponseWriter, r *http.Request) {
	userList, err := mongo.ListUsers()
	if err != nil {
		code := http.StatusInternalServerError
		http.Error(w, err.Error(), code)
		return
	}

	userListJson, _ := json.Marshal(userList)

	w.WriteHeader(http.StatusAccepted)
	w.Write(userListJson)
}
