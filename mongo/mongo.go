package mongo

import (
	"context"
	"fmt"
	"log"

	"mongo/model"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func createClient() (*mongo.Client, context.Context, error) {
	var cred options.Credential

	cred.AuthSource = "admin"
	cred.Username = "root"
	cred.Password = "password"
	mongoURI := "mongodb://localhost:27017"
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI).SetAuth(cred))
	if err != nil {
		log.Printf(`MongoDB can't create the client %s `, err)
		return client, nil, err
	}
	ctx := context.Background()
	err = client.Connect(ctx)
	if err != nil {
		log.Printf(`MongoDB can't connect %s `, err)
		return client, nil, err
	}
	return client, ctx, nil
}

func CreateUser(newUser model.User) (interface{}, error) {
	client, ctx, err := createClient()
	if err != nil {
		log.Panicln("Error creating mongodb client", err)
		return nil, err
	}
	demogasDb := client.Database("test")
	usersCollection := demogasDb.Collection("users")

	emailCheck := usersCollection.FindOne(ctx, bson.M{"email": newUser.Email})
	usernameCheck := usersCollection.FindOne(ctx, bson.M{"username": newUser.Username})
	if emailCheck.Err() == nil {
		return nil, fmt.Errorf("user with email: %s already exists", newUser.Email)
	} else if usernameCheck.Err() == nil {
		return nil, fmt.Errorf("user with username: %s already exists", newUser.Username)
	}
	result, err := usersCollection.InsertOne(ctx, newUser.ToBSON())
	if err != nil {
		return nil, fmt.Errorf("error inserting a new document - %s", err)
	}
	fmt.Println("Result:", result.InsertedID)
	defer client.Disconnect(ctx)
	return result.InsertedID, nil
}

func ListUsers() ([]model.User, error) {
	client, ctx, err := createClient()
	if err != nil {
		fmt.Println("error creating mongodb client", err)
		return nil, err
	}
	demogasDb := client.Database("test")
	usersCollection := demogasDb.Collection("users")
	var results []model.User

	findOptions := options.Find()
	cursor, err := usersCollection.Find(context.TODO(), bson.D{}, findOptions)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	for cursor.Next(context.TODO()) {
		var elem model.User
		err := cursor.Decode(&elem)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}

		results = append(results, elem)
	}

	if err := cursor.Err(); err != nil {
		fmt.Println(err)
		return nil, err
	}

	cursor.Close(ctx)
	defer client.Disconnect(ctx)
	return results, nil
}
