package model

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
)

type User struct {
	Id       string `json:"_id" bson:"_id"`
	Name     string `json:"name" bson:"name"`
	Username string `json:"username" bson:"username"`
	Email    string `json:"email" bson:"email"`
	Password string `json:"password" bson:"password"`
}

func (n *User) ToBSON() []byte {
	data, err := bson.Marshal(n)

	if err != nil {
		fmt.Println("Serialization of Bson failed")
		return nil
	}

	return data
}
